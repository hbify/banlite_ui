import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';

import { LoginService } from './login.service';
import decode from 'jwt-decode';

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {

    const token = localStorage.getItem('currentUser');

    // decode the token to get its payload
    const tokenPayload = decode(token);

    if (tokenPayload.role !== 'admin') {
      localStorage.removeItem('currentUser');
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
