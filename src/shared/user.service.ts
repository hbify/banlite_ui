import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map';

import decode from 'jwt-decode';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {
  private baseUrl = environment.baseUrl;
  private user_url = 'signup';
  private user_url_update = 'user/update'
  private user_by_id = 'user/';

  private all_user = 'users/';
  private delete_user = 'user/remove';

  constructor(private _http: HttpClient, private router: Router) {}
  //get all user


  getUser() {
    const url = this.baseUrl + this.all_user;
    let headers = new Headers();
    headers.append('Authorization', localStorage.getItem('currentUser'));

    return this._http.get(url)
  }
  //add single user
  addUser(key) {
    const url = this.baseUrl + this.user_url;
    return this._http.post(url, key);
  }

  //get user by id
  getUserById(key) {
    const url = this.baseUrl + this.user_by_id;
    return this._http.post(url, key);
  }
  //update user
  updateUser(key: any) {
    const url = this.baseUrl + this.user_url_update;
    return this._http.put(url, key);
  }
  //delete user
  deleteUser(key: any) {
    const url = this.baseUrl + this.delete_user;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: key
    }
    return this._http.delete(url, httpOptions);
  }
}
