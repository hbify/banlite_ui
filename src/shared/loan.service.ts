import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import decode from 'jwt-decode';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoanService {
  private baseUrl = environment.baseUrl;
  private loan_url = 'loan';
  private loan_apply_url = 'loan/apply';
  private loan_generate_url = 'loan/generate';
  private loan_approve_url = 'loan/approve';
  private loan_update_url = 'loan/update';

  private installent_url = 'installment/loan';
  private install_pay_url = 'installment/pay';

  private install_update_url = 'transaction';
  private vuocher_url = 'vuocher/create';


  constructor(private _http: HttpClient, private router: Router) {}

  //payInstallment
  updateInstallment(key) {
    const url = this.baseUrl + this.install_update_url;
    return this._http.post(url, key);
  }

  //payInstallment
  payInstallment(key) {
    const url = this.baseUrl + this.install_pay_url;
    return this._http.post(url, key);
  }

  //get all loan
  getInstallment(key) {
    const url = this.baseUrl + this.installent_url;
    return this._http.post(url, key);
  }
  //get all loan
  getloans() {
    const url = this.baseUrl + this.loan_url;
    return this._http.get(url);
  }

  //add single user
  applyLoan(key) {
    const url = this.baseUrl + this.loan_apply_url;
    return this._http.post(url, key);
  }

  //generate loan
  generateLoan(key) {
    const url = this.baseUrl + this.loan_generate_url;
    return this._http.post(url, key);
  }
  approveLoan(key) {
    const url = this.baseUrl + this.loan_approve_url;
    return this._http.post(url, key);
  }
  //update Voucher
  updateLoan(key: any) {
    const url = this.baseUrl + this.loan_update_url;
    return this._http.put(url, key);
  }
  //delete user
  deleteLoan(key: any) {
    /*let headers = new Headers();
    headers.append('Authorization',  localStorage.getItem('currentUser'));
    return this.http.delete(this.delete_user, {
      body: key,
      headers: headers
    });*/
  }
}
