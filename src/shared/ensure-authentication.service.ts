import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';

import decode from 'jwt-decode';

@Injectable()
export class EnsureAuthenticationService implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {


    const token = localStorage.getItem('currentUser');

    // decode the token to get its payload
    const tokenPayload = decode(token);

    if (tokenPayload.role !== 'employee') {
      localStorage.removeItem('currentUser');
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
