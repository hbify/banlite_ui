import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';

import decode from 'jwt-decode';
import 'rxjs/add/operator/catch';

@Injectable()
export class FinanceService {
  private baseUrl = environment.baseUrl;
  private account_url = 'accounts';
  private acount_deposit = 'voucher/create';
  private account_byid = 'voucher/show';
  private vouchers_url = 'vouchers';
  private select_acc_url = 'account/system';

  private voucher_update_url = 'voucher/update';

  private filter_url = 'voucher/filter';

  constructor(private _http: HttpClient, private router: Router) {}

  selectAccount() {
    const url = this.baseUrl + this.select_acc_url;
    return this._http.get(url);
  }
  //get all user
  getAccounts() {
    const url = this.baseUrl + this.account_url;
    return this._http.get(url);
  }

  //get all voucher
  getVouchers() {
    const url = this.baseUrl + this.vouchers_url;
    return this._http.get(url);
  }
  //add single user
  addDeposit(key) {
    const url = this.baseUrl + this.acount_deposit;
    return this._http.post(url, key);
  }

  //get user by id
  getAccountById(key) {
    const url = this.baseUrl + this.account_byid;
    return this._http.post(url, key);
  }
  //update Voucher
  updateVoucher(key: any) {
    const url = this.baseUrl + this.voucher_update_url;
    return this._http.put(url, key);
  }
  //delete user
  deleteUser(key: any) {
    /*let headers = new Headers();
    headers.append('Authorization',  localStorage.getItem('currentUser'));
    return this.http.delete(this.delete_user, {
      body: key,
      headers: headers
    });*/
  }
  //filter
  getFilter(key) {
    const url = this.baseUrl + this.account_byid;
    return this._http.post(url, key);
  }
}
