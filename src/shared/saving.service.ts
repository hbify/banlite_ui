import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import decode from 'jwt-decode';
import 'rxjs/add/operator/catch';

@Injectable()
export class SavingService {
  private baseUrl = environment.baseUrl;
  private saving_url = 'savings';
  private saving_update_url = 'saving/update';
  private saving_create_url = 'saving/create';
  private saving_genrate = 'saving/generate';
  private install_pay_url = 'installment/paysaving';

  private installent_url = 'installment/saving';
  private install_update_url = 'transaction';



  constructor(private _http: HttpClient, private router: Router) {}

  getInstallment(key) {
    const url = this.baseUrl + this.saving_create_url;
    return this._http.post(url, key);
  }
  //get all user
  getSavings() {
    const url = this.baseUrl + this.saving_url;

    return this._http.get(url);
  }
  planSaving(key) {
    const url = this.baseUrl + this.saving_create_url;
    return this._http.post(url, key);
  }
  //generate saving
  generateSaving(key) {
    const url = this.baseUrl + this.saving_genrate;
    return this._http.post(url, key);
  }
  //
  getInstallmentView(key) {
    const url = this.baseUrl + this.installent_url;
    return this._http.post(url, key);
  }
  //update Saving
  updateSaving(key: any) {
    const url = this.baseUrl + this.saving_update_url;
    return this._http.put(url, key);
  }
  updateInstallment(key) {
    const url = this.baseUrl + this.install_update_url;
    return this._http.post(url, key);
  }
  payInstallment(key) {
    const url = this.baseUrl + this.install_pay_url;
    return this._http.post(url, key);
  }
}
