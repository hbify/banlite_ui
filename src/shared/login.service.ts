import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import decode from 'jwt-decode';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {

  private baseUrl = environment.baseUrl;
  private longin_url = 'signin';
  private show_url = 'user';


 private customer_url = 'customers';
 private customer_byId_url= 'customer';
 private add_customer_url = 'customer/create';
 private transaction_url = 'transaction';

 public token: string;

  constructor(private router: Router,
              private _http: HttpClient) {
  }
  getToken(): string {
    return localStorage.getItem('currentUser');
  }
  login(login_data) {
    const url = this.baseUrl + this.longin_url;
    return this._http.post(url, login_data);
  }
  getCustomer() {
    const url = this.baseUrl + this.customer_url;
    return this._http.get(url);

  }
 getCustomerById(key) {
   const url = this.baseUrl + this.customer_byId_url;
   return this._http.post(url, key);

 }
addCustomer(key) {
  const url = this.baseUrl + this.add_customer_url;
  return this._http.post(url, key);
}
  addTransaction(key) {
    const url = this.baseUrl + this.transaction_url;
    return this._http.post(url, key);
  }
  logout() {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.router.navigateByUrl('');
  }
}
