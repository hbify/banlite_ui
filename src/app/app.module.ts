import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../shared/token.intercepter';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './routing';
import { DataTablesModule } from 'angular-datatables';

import { LoginService } from '../shared/login.service';
import { RoleGuardService } from '../shared/role-guard.service';
import { EnsureAuthenticationService } from '../shared/ensure-authentication.service';
import { UserService } from '../shared/user.service';
import { FinanceService } from '../shared/finance.service';
import { LoanService } from '../shared/loan.service';
import { SavingService } from '../shared/saving.service';

import { AlertCenterModule } from 'ng2-alert-center';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { NgxElectronModule } from 'ngx-electron';

import { AppComponent } from './app.component';
import { FrontComponent } from './components/front/front.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoanComponent } from './components/loan/loan.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AdminNavbarComponent } from './components/admin-navbar/admin-navbar.component';
import { FinanceComponent } from './components/finance/finance.component';
import { SavingComponent } from './components/saving/saving.component';
import { VoucherComponent } from './components/voucher/voucher.component';

import { StatusFilterPipe } from '../shared/statusFilter';


@NgModule({
  declarations: [
    AppComponent,
    FrontComponent,
    LoginComponent,
    NavbarComponent,
    LoanComponent,
    AdminDashboardComponent,
    AdminNavbarComponent,
    FinanceComponent,
    SavingComponent,
    VoucherComponent,
    StatusFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpClientModule,
    DataTablesModule,
    AppRoutingModule,
    NgxElectronModule,
    AlertCenterModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],
  providers: [ LoginService, RoleGuardService, EnsureAuthenticationService, UserService, FinanceService, LoanService, SavingService,StatusFilterPipe,
              {
               provide: HTTP_INTERCEPTORS,
               useClass: TokenInterceptor,
               multi: true
             }],
  bootstrap: [AppComponent]
})
export class AppModule { }
