import {Component, OnInit, ViewChild} from '@angular/core';
import { LoanService } from '../../../shared/loan.service';
import { FinanceService } from '../../../shared/finance.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css'],
  providers: [NgbModal]
})
export class LoanComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  instKeyModel = {'ad': null,
                   'Loans': [
                     {'id': null,
                       'AccountId': 42},
                   ]
                 };
  loanDataToDisplay;
  loanModel = {
       'installmentsNumber': 24,
       'amount': 10000,
       'interestRate': 7,
       'AccountId': 1222,
       'loanStatus': 1
     }
  loanApproveModel = {'loanId': null}
  loanGenerateModel = {'loanId': null}
  loanInst = {'loanId': null}
  payInst = {
    'id': 73,
    'installment': {
      'status': 2
    }
  }
  loanUp = {
    'vType': 0,
    'amount': 0,
    'type': 'deposit',
    'accId': 0,
    'accountId': 1199

  }
  loanUp2 = {
    'voucher': {
      'AccountId': 38,
      'vType': 0,
      'amount': 2000
    },
    'transaction': {
      'AccountId': 43,
      'transactionType': 'deposit',
      'amount': 3000
    }
  }
  loanWithdraw = {
    'voucher': {
      'AccountId': 38,
      'vType': 1,
      'amount': 2000
    },
    'transaction': {
      'AccountId': 38,
      'transactionType': 'withdraw',
      'amount': 3000
    }

  }
  withdrawModel = {
    'vType': 1,
    'amount': 1000,
    'type': '',
    'accId': 1222,
    'accountId': 0

  }

 temkKey;
  generateClicked = false;
  installmentData;
  installTempmentData;
  selectValue;

  constructor(private loanSer: LoanService, private finServ: FinanceService,
              private modalService: NgbModal) { }

  selectAccount() {
    return this.finServ.selectAccount().subscribe(
      res => {
        const data = res;
        this.selectValue = data;
      });
  }
     ngOnInit() {
       this.selectAccount();
       this.displayLoan();
     }
    displayLoan() {
      this.dtOptions = {
        pagingType: 'full_numbers',
        retrieve: true,
        orderCellsTop: false,
        lengthMenu: [ 25, 50, 75, 100 ],

      };
      this.loanSer.getloans().subscribe(
        res => {
          const data = res;
          this.loanDataToDisplay = data;
          console.log(this.loanDataToDisplay);
          this.generateClicked = true;
          this.dtTrigger.next();
        });
    }

  applyLoan(key) {
  this.loanModel.AccountId = key;
   //console.log(key);
  }
  saveLoan() {
    /*this.loanWithdraw.amount = this.loanModel.amount;
    this.loanWithdraw.AccountId = this.loanModel.AccountId;*/
    return this.loanSer.applyLoan(this.loanModel).subscribe(
      res => {
           const data = res;
           this.displayLoan();
    }
    )
  }
  approve(key) {
    this.loanApproveModel.loanId = key;
    /*this.loanSer.approveLoan(this.loanApproveModel).subscribe(
      (response: Response) => this.confirm(response),
      (err) => this.fail(err)
    )*/
  }

  generate(key) {
    this.loanGenerateModel.loanId = key;
    this.loanSer.generateLoan(this.loanGenerateModel).subscribe(
      response => this.confirm(response),
      err => this.fail(err));
  }
  confirm(key) {
    this.displayLoan();
    //window.location.reload();
  }
  fail(key) {
    console.log(key);
  }
  installmentView(key) {
   this.temkKey = key;
    this.loanUp2.transaction.AccountId = key.id;
    console.log(this.loanUp2);
    this.loanUp.accId = key.id;

    this.loanInst.loanId = key.Loans[0].id;

    this.loanSer.getInstallment(this.loanInst).subscribe(
      res => {
        const data = res;
        this.installmentData = data;
        console.log(this.installmentData);
      });
  }
  payInstallment(key) {
    this.payInst.id = key;
    this.loanSer.payInstallment(this.payInst).subscribe(
      res => {
        this.installTempmentData = res;
        console.log(this.temkKey);
        this.installmentView(this.temkKey);
      }
    );
  }
  updateInstallment(key) {
    this.loanUp.amount = key;
    this.loanUp2.transaction.amount = key;
    this.loanUp2.voucher.amount = key;
    console.log(this.loanUp2);

    this.loanSer.updateInstallment(this.loanUp2).subscribe(
      res => {
        this.installmentView(this.instKeyModel.Loans[0]);
      });
  }

  createInstallment(key, id, actId) {
    this.loanWithdraw.transaction.amount = this.loanModel.amount;
    this.loanWithdraw.voucher.amount = this.loanModel.amount;
    this.loanWithdraw.transaction.AccountId = actId;
    this.loanApproveModel.loanId = id;
  }

  create() {
    this.loanWithdraw.voucher.AccountId = this.withdrawModel.accountId;
    console.log(this.loanWithdraw);
    this.loanSer.approveLoan(this.loanApproveModel).subscribe(
      response => this.confirm(response),
      (err) => this.fail(err));

    this.loanSer.updateInstallment( this.loanWithdraw).subscribe(
      res => {
        this.installmentView(this.loanInst.loanId);
      });
  }

  print(): void {
    window.print();
  }
  openDepositWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openWithdrawWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openTransactionWindows(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
