import { Component, OnInit, Input } from '@angular/core';
import { FinanceService } from '../../../shared/finance.service';
import { UserService } from '../../../shared/user.service';
import decode from 'jwt-decode';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {
  @Input() filterBy? = 'all';
  voucherData;
  roleCheck;

  userByid = { 'id': null }
  author = {
    'id': 1191,
    'voucher': {
        'authorisedBy': 4
        }
     }

  conf = {
    'id': 1191,
    'voucher': {
      'confirmedBy': 4
    }
  }

  constructor(private finService: FinanceService, private userService: UserService) { }

  ngOnInit() {
    this.getVouchers();
  }

 getVouchers() {
   const token = localStorage.getItem('currentUser');
   const tokenPayload = decode(token);
   this.userByid.id = tokenPayload.sub;

   this.userService.getUserById(this.userByid).subscribe(
     res => {
       const data = res;
       this.roleCheck = data;
       console.log(this.roleCheck);
     })

    return this.finService.getVouchers().subscribe(
      res => {
        const data = res;
        this.voucherData = data;
        console.log(this.voucherData);
      });
 }

 authorize(key) {
   const token = localStorage.getItem('currentUser');
   const tokenPayload = decode(token);

   this.author.id = key;
   this.author.voucher.authorisedBy = tokenPayload.sub;

   this.finService.updateVoucher(this.author).subscribe(
     res => this.confirmData(res),
     err => this.fail(err));
    }
  confirmData(key) {
    window.location.reload();
  }
  fail(key) {
    console.log(key);
  }
  confirm(key) {
    const token = localStorage.getItem('currentUser');
    const tokenPayload = decode(token);

    this.conf.id = key;
    this.conf.voucher.confirmedBy = tokenPayload.sub;

    this.finService.updateVoucher(this.conf).subscribe(
      res => this.confirmData(res),
      err => this.fail(err));
  }
  print() {

  }
}
