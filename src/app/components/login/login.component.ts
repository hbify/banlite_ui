import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../../shared/login.service';
import decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model = {
    email: '',
    password: ''
  };
  loading = false;
  error = '';
  logData = 0;
  data;
  constructor(
    private router: Router,
    private auth: LoginService) {

  }

  ngOnInit() {
    // reset login status
    this.auth.logout();
  }

  login() {
    this.auth.login(this.model)
      .subscribe(
        (data) => this.saveToken(data),
        (err) => this.errorToken(err));
  }
  errorToken(e) {
    if(e){
      this.loading = true;
      this.error = 'Username  or password is wrong.';
    }
  }


  saveToken(token) {
    let token1 = token.token;
    console.log(token1);
    const tokenPayload = decode(token1);
    localStorage.setItem('currentUser', token1);
    if (token) {
      const token = localStorage.getItem('currentUser');
      const tokenPayload = decode(token);
      if (tokenPayload.role === 'employee') {
        this.router.navigate(['/finance']);
      }
      else if (tokenPayload.role === 'admin') {
        this.router.navigate(['/admin']);
      }
    }
  }
}
