import {Component, OnInit, ViewChild} from '@angular/core';
import { SavingService } from '../../../shared/saving.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-saving',
  templateUrl: './saving.component.html',
  styleUrls: ['./saving.component.css'],
  providers: [ NgbModal ]
})
export class SavingComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  savingData;

  savingGenerateModel = {'savingId': null}
  savingInstallmentModel = {'savingId': null}
  saveModel = {
    'installmentsNumber': 24,
    'amount': 10000,
    'interestRate': 7,
    'AccountId': 1222
  }
  instKeyModel = {'ad': null,
    'Savings': [
      {'id': null,
        'AccountId': 42},
    ]
  };

  payInst = {
    'id': 73,
    'installment': {
      'status': 2
    }
  }
  saveUp = {
    'vType': 0,
    'amount': 0,
    'type': 'deposit',
    'accId': 0,
    'accountId': 1199

  }
  saveUp2 = {
    'voucher': {
      'AccountId': 82,
      'vType': 0,
      'amount': 2000
    },
    'transaction': {
      'AccountId': 43,
      'transactionType': 'deposit',
      'amount': 3000
    }
  }
  installTempmentData;
  installmentData;
  tempKey;

  constructor(private saveService: SavingService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getSavings();
  }
 getSavings() {
   this.dtOptions = {
     pagingType: 'full_numbers',
     retrieve: true,
     orderCellsTop: false,
     lengthMenu: [ 25, 50, 75, 100 ],

   };
    this.saveService.getSavings().subscribe(
      res => {
        const data = res;
        this.savingData = data;
        this.dtTrigger.next();
        console.log(this.savingData);
      });
 }

 createInstallment(key) {
  this.saveModel.AccountId = key;
 }

  createSave() {
    this.saveService.getInstallment(this.saveModel).subscribe(
      res => this.confirmCreate(res),
           err => this.fail(err));
  }

  confirmCreate(key) {
    this.getSavings();
  }
  failCreate(key) {
    console.log(key);
  }

  generate(key) {
    this.savingGenerateModel.savingId = key;
    console.log(this.savingGenerateModel);
    this.saveService.generateSaving(this.savingGenerateModel).subscribe(
      response => this.confirm(response),
      err => this.fail(err));
  }

  installmentView(key) {
    this.tempKey = key;
    this.saveUp2.transaction.AccountId = key.id;
    this.saveUp.accId = key.id;
    this.savingInstallmentModel.savingId = key.Savings[0].id;
    this.saveService.getInstallmentView(this.savingInstallmentModel).subscribe(
      res => {
        const data = res;
        this.installmentData = data;
        console.log(this.installmentData);
      });
  }


  confirm(key) {
    this.getSavings();
  }
  fail(key) {
    console.log(key);
  }

  payInstallment(key) {
    this.payInst.id = key;
    this.saveService.payInstallment(this.payInst).subscribe(
      res => {
        this.installTempmentData = res;
        this.installmentView(this.tempKey);
      }
    );
  }
  updateInstallment(key) {
    this.saveUp.amount = key;
    this.saveUp2.transaction.amount = key;
    this.saveUp2.voucher.amount = key;
    console.log(this.saveUp2);

    this.saveService.updateInstallment(this.saveUp2).subscribe(
      res => {
        this.installmentView(this.tempKey);
      });
  }
  print() {

  }
  openDepositWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openWithdrawWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openTransactionWindows(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
