import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../shared/login.service';


@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.css']
})
export class AdminNavbarComponent implements OnInit {
  constructor(private auth: LoginService) { }

  ngOnInit() {
  }
  logout() {
    this.auth.logout();
  }

}
