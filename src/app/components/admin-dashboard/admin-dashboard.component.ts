import {Component, OnInit, ViewChild} from '@angular/core';
import { UserService } from '../../../shared/user.service';
import { AlertCenterService } from 'ng2-alert-center';
import {Alert } from 'ng2-alert-center';
import { AlertType } from 'ng2-alert-center';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  placement = 'left';
  popoverTitle = '';
  popoverMessage = 'Are you sure <b> do you want to delete this?';
  confirmText = 'Delete <i class="glyphicon glyphicon-ok"></i>';
  cancelText = 'Cancel <i class="glyphicon glyphicon-remove"></i>';
  confirmClicked = false;
  cancelClicked = false;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  userModel= {
    'email': '',
    'firstNames': '',
    'lastName': '',
    'password': '',
    'title': '',
    'role': ''
  }
  userUpdateModel= {
    'id': null,
    'user': {
      'email': '',
      'firstNames': '',
      'lastName': '',
      'password': '',
      'title': '',
      'role': ''
    }
  }
  userRole =  [
    {
      'ID': 'admin',
      'ROLE': 'Admin'
    },
    {
      'ID': 'employee',
      'ROLE': 'Employe'
    }
  ];

  userTitle =  [
    {
      'ID': 'secretary',
      'TITLE': 'Secretary'
    },
    {
      'ID': 'cashier',
      'TITLE': 'Cashier'
    },
    {
      'ID': 'super',
      'TITLE': 'Supervisor'
    }
  ];
  deleteUser = {'id': null }
  updateUser = {'id': null }
  userData;
  updateTempData;
  //{ email: password: role: employee firstNames: lastName: title: secretary, cashier, supervsior }
  constructor(private userService: UserService,
              private service: AlertCenterService) { }

  ngOnInit() {
    this.getUserData();
  }
  getUserData() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      lengthMenu: [ 25, 50, 75, 100, 1000 ],

    };
   return this.userService.getUser().subscribe(
     res => {
       const data = res;
       this.userData = data;
       this.dtTrigger.next();
     });
  }

  confirmAdd(key) {
    this.deleteAlert();
    window.location.reload();
  }
  failedAdd(key) {
    console.log('failed' + key);
  }
  addUser() {
    return this.userService.addUser(this.userModel).subscribe(
      response => this.confirmAdd(response),
      err => this.failedAdd(err));
  }
 confirmDelete(key) {
    this.deleteAlert();
    window.location.reload();
  }
  failedDelete(key) {
    console.log('failed' + key);
  }

  deletedUser( key) {
     this.deleteUser.id = key;
     console.log(this.deleteUser);
      this.userService.deleteUser(this.deleteUser).subscribe(
        response => this.confirmDelete(response),
        err => this.failedDelete(err));
  }
  updatedUser() {
    console.log(this.userUpdateModel);
    return this.userService.updateUser(this.userUpdateModel).subscribe(
      response => this.confirmUpdate(response),
      err => this.failedUpdate(err)
    )
  }

  confirmUpdate(key) {
    this.editAlert();
    window.location.reload();
  }
  failedUpdate(key) {
    console.log('failed' + key);
  }

  editUser(key) {
    this.updateUser.id = key;

    return this.userService.getUserById(this.updateUser).subscribe(
      res => {
        const data = res;
        this.updateTempData = data;
        this.userUpdateModel.id = this.updateTempData.id;
        this.userUpdateModel.user.email = this.updateTempData.email;
        this.userUpdateModel.user.firstNames = this.updateTempData.firstNames;
        this.userUpdateModel.user.lastName = this.updateTempData.lastName;
        this.userUpdateModel.user.password = this.updateTempData.password;
        this.userUpdateModel.user.role = this.updateTempData.role;
        this.userUpdateModel.user.title = this.updateTempData.title;

      }
    )
    //console.log(key);
  }

  tarkistetuAlert() {
    const alert = Alert.create(AlertType.SUCCESS, 'Tila päivitetty onnistuneesti', 1000);
    this.service.alert(alert);
  }
  editAlert() {
    const alert = Alert.create(AlertType.SUCCESS, 'Tiedot päivitetty onnistuneesti', 1000);
    this.service.alert(alert);
  }
  deleteAlert() {
    const alert = Alert.create(AlertType.SUCCESS, 'Poisto tehty onnistuneesti', 1000);
    this.service.alert(alert);
  }

  sendAnAutoDismissingAlert() {
    const alert = Alert.create(AlertType.SUCCESS, 'Talleta tiedot', 1000);
    this.service.alert(alert);
  }
}
