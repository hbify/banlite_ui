import { Component, OnInit } from '@angular/core';
import { FinanceService } from '../../../shared/finance.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import decode from 'jwt-decode';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css'],
  providers: [ NgbModal ]
})
export class FinanceComponent implements OnInit {
  closeResult: string;
  accountData;
  depositModel = {
    'vType': 0,
    'AccountId': 1185,
    'amount': 2000,
    'preparedBy': ''
  }
  withdrawModel = {
    'vType': 1,
    'AccountId': 0,
    'amount': 2000,
    'preparedBy': ''
  };
  transactionData;
  transactionClicked = false;
  transactionByid = { 'AccountId': null }

  constructor(private finService: FinanceService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.getAccount();
  }
 getAccount() {
    return this.finService.getAccounts().subscribe(
      res => {
        const data = res;
        this.accountData = data;
      });
 }

  getTransactionById(key) {
    this.transactionByid.AccountId = key;
    console.log(this.transactionByid);
     return this.finService.getAccountById(this.transactionByid).subscribe(
       res =>{
         const data = res;
         this.transactionData = data;
         this.transactionClicked = true;
       });
  }
  addCustomer(key) {

  }
  addTransaction(key) {
    this.finService.addDeposit(key).subscribe(
      response => this.confirmSave(response),
      (err) => this.ignoreSave(err));
  }
  confirmSave(key) {
    console.log(key);
    window.location.reload();
  }
  ignoreSave(key) {
    console.log('failed' + key);
  }
  getId(key) {

  }

  deposit(key) {
    const token = localStorage.getItem('currentUser');
    const tokenPayload = decode(token);

    this.depositModel.preparedBy = tokenPayload.sub;
    this.depositModel.AccountId = key;
  }
  saveDeposit() {

    this.addTransaction(this.depositModel);
    console.log(this.depositModel);
  }

  withDraw(key) {
    const token = localStorage.getItem('currentUser');
    const tokenPayload = decode(token);

    this.withdrawModel.preparedBy = tokenPayload.sub;
    this.withdrawModel.AccountId = key;
  }
  saveWithDraw() {
    this.addTransaction(this.withdrawModel);
    console.log(this.withdrawModel);
  }

  openDepositWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openWithdrawWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openTransactionWindows(content) {
    this.modalService.open(content, { size: 'lg' });
  }

}
