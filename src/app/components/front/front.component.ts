import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LoginService } from '../../../shared/login.service';
import { FinanceService } from '../../../shared/finance.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.css'],
  providers: [ NgbModal ]
})
export class FrontComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  customerData;
  singleCustomer;
  customerAccount;
  customerTranasaction;
  selectValue;
  accid;
  transactionClicked = false;
  customerId = {
    'id': ''
  }

  depositModel = {
    'voucher': {
      'AccountId': 82,
      'vType': 0,
      'amount': 2000
    },
    'transaction': {
      'AccountId': 43,
      'transactionType': 'deposit',
      'amount': 3000
    }
  }
  withdrawModel = {
    'voucher': {
      'AccountId': 82,
      'vType': 1,
      'amount': 2000
    },
    'transaction': {
      'AccountId': 43,
      'transactionType': 'credit',
      'amount': 3000
    }

  }
  customerModel= {
     customer: { 'firstNames': '',
       'lastName': ''
      },
       'accNo': ''
  }
  transactionModel = {
    'type': '',
    'amount': 0,
    'accId': 0
  }
  constructor( private dataSer: LoginService, private finSer: FinanceService,
               private modalService: NgbModal) { }

  ngOnInit(): void {
    this.selectAccount();
    this.getCustomerData();
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  }

  getCustomerData() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      retrieve: true,
      orderCellsTop: false,
      lengthMenu: [ 25, 50, 75, 100 ],

    };

    this.dataSer.getCustomer().subscribe(
      response => {
        const data = response;
        this.customerData = data;
        this.dtTrigger.next();
      });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.getCustomerData();
    });
  }
  getCustomerById(key) {
    return this.dataSer.getCustomerById(key).subscribe(
      res => {
        const data = res;
        this.singleCustomer = data;
        this.customerAccount = this.singleCustomer.Accounts;
        console.log(this.customerAccount);
        this.accid = this.customerAccount[0].id;
        this.transactionModel.accId = this.accid;
        this.transactionClicked = true;
        return this.singleCustomer;
      });
  }
  addCustomer(key) {
    this.dataSer.addCustomer(key).subscribe(
      response => this.confirmSaveCuusromer(response),
      err => this.ignoreSaveCustomer(err));
  }
  confirmSaveCuusromer(key) {
    this.getCustomerData();
  }
  ignoreSaveCustomer(key) {
    console.log('failed' + key);
  }
  addTransaction(key) {
    this.dataSer.addTransaction(key).subscribe(
      response => this.confirmSave(response),
      err => this.ignoreSave(err));
  }
  confirmSave(key) {
    this.rerender();
  }
  ignoreSave(key) {
    console.log('failed' + key);
  }
 getId(key) {
    this.customerId.id = key;
    this.getCustomerById(this.customerId);
 }
 addCust() {
   this.addCustomer(this.customerModel);
 }
 deposit(key, type) {
   this.selectAccount();
   this.customerId.id = key;
   this.transactionModel.type = type;
   this.depositModel.transaction.AccountId = key;
 }
 saveDeposit() {
   this.depositModel.voucher.amount = this.depositModel.transaction.amount;
   this.addTransaction(this.depositModel);
 }

  withDraw(key, type) {
    this.customerId.id = key;
    this.transactionModel.type = type;
    this.withdrawModel.transaction.AccountId = key;
  }
  saveWithDraw() {
    this.withdrawModel.voucher.amount = this.withdrawModel.transaction.amount;
    console.log(this.withdrawModel);
    //this.addTransaction(this.withdrawModel);
  }
  selectAccount() {
    return this.finSer.selectAccount().subscribe(
      res => {
        const data = res;
        this.selectValue = data;
      });
  }

  openDepositWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openWithdrawWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openCustomerWindows(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  openTransactionWindows(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
