import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { FrontComponent } from './components/front/front.component';
import { LoanComponent } from './components/loan/loan.component';
import { RoleGuardService } from '../shared/role-guard.service';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { EnsureAuthenticationService } from '../shared/ensure-authentication.service';
import { FinanceComponent } from './components/finance/finance.component';
import { SavingComponent } from './components/saving/saving.component';
import { VoucherComponent } from './components/voucher/voucher.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'admin',
        component: AdminDashboardComponent,
        canActivate: [RoleGuardService]
      },
      {
        path: 'front',
        component: FrontComponent,
        canActivate: [EnsureAuthenticationService]
      },
      {
        path: 'loan',
        component: LoanComponent,
        canActivate: [EnsureAuthenticationService]
      },
      {
        path: 'finance',
        component: FinanceComponent,
        canActivate: [EnsureAuthenticationService]
      },
      {
        path: 'saving',
        component: SavingComponent,
        canActivate: [EnsureAuthenticationService]
      },
      {
        path: 'voucher',
        component: VoucherComponent,
        canActivate: [EnsureAuthenticationService]
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      }
    ])
  ],
  providers: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
